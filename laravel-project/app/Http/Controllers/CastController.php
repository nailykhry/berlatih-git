<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function index()
    {
        return view('cast.index', [
            "title" => "Daftar Cast",
            "cast" => Cast::latest(),
        ]);
    }

    public function create()
    {
        return view('cast.create', [
            "title" => "Tambah"
        ]);
    }

    public function show($id)
    {
        $cast = Cast::findOrFail($id);
        return view('cast.index', compact('cast'), [
            "title" => "ID TERTENTU",
            "cast" => $cast
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = Cast::create([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);

        if ($cast) {
            return redirect()
                ->route('cast.index')
                ->with([
                    'success' => 'Cast berhasil ditambahkan'
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Terdapat kesalahan, coba lagi!'
                ]);
        }
    }


    public function edit($id)
    {
        $cast = Cast::findOrFail($id);
        return view('cast.edit', compact('cast'), [
            "title" => "Edit"
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = Cast::findOrFail($id);
        $cast->update([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);

        if ($cast) {
            return redirect()
                ->route('cast.index')
                ->with([
                    'success' => 'Cast berhasil diperbarui'
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Terdapat kesalahan, coba lagi!'
                ]);
        }
    }


    public function destroy($id)
    {
        $cast = Cast::findOrFail($id);
        $cast->delete();

        if ($cast) {
            return redirect()
                ->route('cast.index')
                ->with([
                    'success' => 'Berhasil menghapus data cast'
                ]);
        } else {
            return redirect()
                ->route('cast.index')
                ->with([
                    'error' => 'Terdapat kesalahan, coba lagi!'
                ]);
        }
    }
}
