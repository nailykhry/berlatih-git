@extends('layout.master')

@section('content')

<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-12">

            <!-- Notifikasi menggunakan flash session data -->
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif

            @if (session('error'))
            <div class="alert alert-error">
                {{ session('error') }}
            </div>
            @endif

            <div class="card border-0 shadow rounded">
                <div class="card-body">
                    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                                value="{{ old('nama', $cast->nama) }}" required>

                            <!-- error message untuk title -->
                            @error('nama')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur"
                                value="{{ old('umur', $cast->umur) }}" required>

                            <!-- error message untuk title -->
                            @error('umur')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="bio">Bio</label>
                            <input type="text" class="form-control @error('bio') is-invalid @enderror" name="Bio"
                                value="{{ old('bio', $cast->bio) }}" required>

                            <!-- error message untuk title -->
                            @error('bio')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-md btn-primary">Update</button>
                        <a href="{{ route('cast.index') }}" class="btn btn-md btn-secondary">back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection