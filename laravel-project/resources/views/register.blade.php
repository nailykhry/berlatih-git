<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BUAT AKUN BARU</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br><br>

        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname"><br><br>

        <label for="gender">Gender:</label><br>
        <input type="radio" id="Male" name="gender" value="Male">
        <label for="Male">Male</label><br>
        <input type="radio" id="Female" name="gender" value="Female">
        <label for="Female">Female</label><br>
        <input type="radio" id="Other" name="gender" value="Other">
        <label for="Other">Other</label><br><br>

        <label for="nationality">Nationality:</label><br>
        <select id="nationality" name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>

        <label for="language">Language Spoken:</label><br>
        <input type="checkbox" id="bahasa" name="language" value="bahasa">
        <label for="Bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" id="English" name="language" value="English">
        <label for="English">English</label><br>
        <input type="checkbox" id="bahasa" name="language" value="Other">
        <label for="Other">Other</label><br><br>

        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea><br>

        <button type="submit">Sign Up</button>
    </form>
</body>

</html>