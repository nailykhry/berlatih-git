<?php 

require('animal.php');
require('frog.php');
require('ape.php');

echo "<h1>RELEASE 0</h1>";

$sheep = new Animal("shaun");

echo "Name : ". $sheep->name . "<br>"; // "shaun"
echo "legs : ".$sheep->legs."<br>"; // 4
echo "cold_blooded : " . $sheep->cold_blooded . "<br>"; // "no"


echo "<h1>RELEASE 1</h1>";

$kodok = new Frog("buduk");
echo "Name : ". $kodok->name . "<br>"; // "shaun"
echo "legs : ".$kodok->legs."<br>"; // 4
echo "cold_blooded : " . $kodok->cold_blooded . "<br>"; // "no"
echo "Jump: ";
$kodok->jump(); // "hop hop"
echo  "<br><br>";


$sungokong = new Ape("kera sakti");
echo "Name : ". $sungokong->name . "<br>"; // "shaun"
echo "legs : ".$sungokong->legs."<br>"; // 4
echo "cold_blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
echo "Yell : ";
$sungokong->yell();



?>